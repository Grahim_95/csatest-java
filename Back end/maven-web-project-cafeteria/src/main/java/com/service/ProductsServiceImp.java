package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.ProductDao;
import com.model.Products;

@Service
@Transactional(readOnly = true)
public class ProductsServiceImp implements ProductsService{

	@Autowired
	private ProductDao productDao;
	
	@Transactional
	public int create(Products product) {
		return productDao.create(product);
	}

	@Transactional
	public void update(Products product, int id) {
		productDao.update(id, product);
	}

	@Transactional
	public void delete(int id) {
		productDao.delete(id);
	}

	public List<Products> list() {
		return productDao.list();
	}

	public Products get(int id) {
		return productDao.get(id);
	}

}
