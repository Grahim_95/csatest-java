package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.OrderDao;
import com.model.Orders;

@Service
@Transactional(readOnly = true)
public class OrderServiceImp implements OrderService{

	@Autowired
	private OrderDao orderDao ;
	
	@Transactional
	public int create(Orders order) {
		return orderDao.create(order);
	}
	
	@Transactional
	public void update(Orders order, int id) {
		orderDao.update(order, id);
	}
	@Transactional
	public void delete(int id) {
		orderDao.delete(id);
	}

	public List<Orders> list() {
		return orderDao.list();
	}

	public Orders get(int id) {
		return orderDao.get(id);
	}

}
