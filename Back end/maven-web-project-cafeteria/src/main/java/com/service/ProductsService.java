package com.service;

import java.util.List;

import com.model.Products;

public interface ProductsService {

	int create(Products product);
	void update(Products product, int id);
	void delete(int id);
	List<Products> list();
	Products get(int id);
}
