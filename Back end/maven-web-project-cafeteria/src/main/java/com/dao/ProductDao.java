package com.dao;

import java.util.List;

import com.model.Products;

public interface ProductDao {
	int create(Products product);
	void update(int id,Products product);
	void delete(int id);
	List<Products> list();
	Products get(int id);
}
