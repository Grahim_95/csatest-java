package com.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.Products;

import Utils.Utilities;

@Repository
public class ProductsDaoImp implements ProductDao{

	@Autowired
	private SessionFactory sessionFactory;
	private Utilities utils = new Utilities();
	
	public int create(Products product) {
		String imageBase64 = product.getImage();
		product.setImage("");
		sessionFactory.getCurrentSession().save(product);
		product.setImage(utils.saveImage(imageBase64,product.getId()));
		sessionFactory.getCurrentSession().update(product);
		return product.getId();
	}

	public void update(int id, Products product) {
		Session session = sessionFactory.getCurrentSession();
		Products updateProduct = session.byId(Products.class).load(id);
		updateProduct.clone(product);
		session.flush();
	}

	public void delete(int id) {
		Session session = sessionFactory.getCurrentSession();
	    Products product = session.byId(Products.class).load(id);
	    session.delete(product);
	}

	public List<Products> list() {
		Session session = sessionFactory.getCurrentSession();
	    CriteriaBuilder cb = session.getCriteriaBuilder();
	    CriteriaQuery<Products> cq = cb.createQuery(Products.class);
	    Root<Products> root = cq.from(Products.class);
	    cq.select(root);
	    Query<Products> query = session.createQuery(cq);
	    return query.getResultList();
	}

	public Products get(int id) {
		return sessionFactory.getCurrentSession().get(Products.class, id);
	}

}
