package com.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.Orders;

@Repository
public class OrderDaoImp implements OrderDao{

	@Autowired
	private SessionFactory sessionFactory;
	

	public int create(Orders order) {
		sessionFactory.getCurrentSession().save(order);
		return order.getId();
	}

	public void update(Orders order, int id) {
		Session session = sessionFactory.getCurrentSession();
		Orders updateOrder = session.byId(Orders.class).load(id);
		updateOrder.clone(order);
		session.flush();
	}

	public void delete(int id) {
		Session session = sessionFactory.getCurrentSession();
		Orders order = session.byId(Orders.class).load(id);
		session.delete(order);		
	}

	public List<Orders> list() {
		Session session = sessionFactory.getCurrentSession();
	    CriteriaBuilder cb = session.getCriteriaBuilder();
	    CriteriaQuery<Orders> cq = cb.createQuery(Orders.class);
	    Root<Orders> root = cq.from(Orders.class);
	    cq.select(root);
	    Query<Orders> query = session.createQuery(cq);
	    return query.getResultList();
	}

	public Orders get(int id) {
		return sessionFactory.getCurrentSession().get(Orders.class, id);
	}

}
