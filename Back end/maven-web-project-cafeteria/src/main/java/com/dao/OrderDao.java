package com.dao;

import java.util.List;

import com.model.Orders;

public interface OrderDao {

	int create(Orders order);
	void update(Orders order, int id);
	void delete(int id);
	List<Orders> list();
	Orders get(int id);
}
