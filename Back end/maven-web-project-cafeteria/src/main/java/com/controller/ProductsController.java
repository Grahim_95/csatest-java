package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.model.Products;
import com.service.ProductsService;

@RestController
public class ProductsController {

	@Autowired
	private ProductsService productsService;
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping("/products")
	public ResponseEntity<?> create(@RequestBody Products products)
	{
		int id = productsService.create(products);
		return ResponseEntity.ok().body(id);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/products/{id}")
	public ResponseEntity<Products> get(@PathVariable("id") int id)
	{
		Products product = productsService.get(id);
		return ResponseEntity.ok().body(product);
	} 
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/products")
	public ResponseEntity<List<Products>> list()
	{
		List<Products> products = productsService.list();
		return ResponseEntity.ok().body(products);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PutMapping("/products/{id}")
	public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody Products product)
	{
		productsService.update(product, id);
		return ResponseEntity.ok().body("Succesfully updated");
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@DeleteMapping("/products/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id)
	{
		productsService.delete(id);
		return ResponseEntity.ok().body("Succesfully eliminated");
	}
}
