package com.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.model.Orders;
import com.service.OrderService;

@RestController
public class OrderController {

	@Autowired
	private OrderService orderService;
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping("/orders")
	public ResponseEntity<?> create(@RequestBody Orders order)
	{		
		int id = orderService.create(order);
		return ResponseEntity.ok().body(id);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/orders/{id}")
	public ResponseEntity<Orders> get(@PathVariable("id") int id)
	{
		Orders order = orderService.get(id);
		return ResponseEntity.ok().body(order);
	} 
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/orders")
	public ResponseEntity<List<Orders>> list()
	{
		List<Orders> orders = orderService.list();
		return ResponseEntity.ok().body(orders);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PutMapping("/orders/{id}")
	public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody Orders order)
	{
		orderService.update(order, id);
		return ResponseEntity.ok().body("Succesfully updated");
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@DeleteMapping("/orders/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id)
	{
		orderService.delete(id);
		return ResponseEntity.ok().body("Succesfully eliminated");
	}
}
