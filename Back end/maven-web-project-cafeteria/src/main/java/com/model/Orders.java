package com.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
@Entity(name = "Orders")
public class Orders {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;		
	@ManyToMany(fetch = FetchType.EAGER,
			cascade = {CascadeType.PERSIST,CascadeType.MERGE})
	private List<Products> productos;	    	
	private String state;
	
	public void clone(Orders order)
	{				
		this.state = order.getState();	
		this.productos = order.getProducts();
	}
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<Products> getProducts() {
		return productos;
	}
	public void setProducts(List<Products> products) {
		this.productos = products;
	}
	
}
