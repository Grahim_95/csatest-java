package Utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Utilities {

	public String saveImage(String image,int id)
	{
		String base64Image = image.split(",")[1];		
		String path = "images/image_id="+String.valueOf(id)+".jpg";
		byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);		
		try {
			BufferedImage img = ImageIO.read(new ByteArrayInputStream(imageBytes));
			File outputfile = new File(path);
			ImageIO.write(img, "jpg", outputfile);
		} catch (IOException e) {			
			e.printStackTrace();
		}
		return path;
	}	
}
