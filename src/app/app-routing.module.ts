import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component'
import { CreateOrderComponent } from './components/create-order/create-order.component'
import { ColaComponent } from './components/cola-list/cola.component'

const routes: Routes = [
  { path: 'Orders', component: AppComponent},
  { path: 'CreateOrders', component: CreateOrderComponent},
  { path: 'Index', component: ColaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
