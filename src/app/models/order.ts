import { Product } from './product'

export class Order {
    id: number;
    products: Product[];
    state: string;
}
