import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductsListComponent } from './components/products-list/products-list.component';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { ProductsService } from './services/products.service';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ColaComponent } from './components/cola-list/cola.component';
import { PreparacionListComponent } from './components/preparacion-list/preparacion-list.component';
import { FinalizadoListComponent } from './components/finalizado-list/finalizado-list.component';
import { EntregadoListComponent } from './components/entregado-list/entregado-list.component';
import { CreateOrderComponent } from './components/create-order/create-order.component'
import { OrderService } from './services/order.service';
import { HelperService } from './services/HelperService';

@NgModule({
  declarations: [
    AppComponent,
    ProductsListComponent,
    ProductFormComponent,
    NavbarComponent,
    ColaComponent,
    PreparacionListComponent,
    FinalizadoListComponent,
    EntregadoListComponent,
    CreateOrderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [ProductsService,OrderService,HelperService],
  bootstrap: [AppComponent]
})
export class AppModule { }
