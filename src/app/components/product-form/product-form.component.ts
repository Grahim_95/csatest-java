import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from '../../services/products.service';
import { Product } from '../../models/product';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  product: Product;
  result: string;

  constructor( 
    private productsService: ProductsService,private router: Router)
  {
    this.product = new Product();
  }

  onSubmit(){
    this.productsService.save(this.product).subscribe(result => this.Successfull());
  }

  Successfull(){
    alert("Succesfully created");
    this.product = new Product();
    this.router.dispose();
  }

  fileChangeEvent(fileInput: any)
  {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      const image = new Image();
      image.src = e.target.result;
      const imgBase64Path = e.target.result;
      this.product.image = imgBase64Path;                                
    }
    reader.readAsDataURL(fileInput.target.files[0]);
  }

  ngOnInit(): void {
  }

}
