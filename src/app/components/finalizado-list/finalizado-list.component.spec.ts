import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalizadoListComponent } from './finalizado-list.component';

describe('FinalizadoListComponent', () => {
  let component: FinalizadoListComponent;
  let fixture: ComponentFixture<FinalizadoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinalizadoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalizadoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
