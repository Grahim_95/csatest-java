import { Component, OnInit } from '@angular/core';
import { Order } from '../../models/order'
import { Product } from '../../models/product'
import { OrderService } from '../../services/order.service'
import { HelperService } from '../../services/HelperService'
import { ProductsService }from '../../services/products.service'

@Component({
  selector: 'app-cola',
  templateUrl: './cola.component.html',
  styleUrls: ['./cola.component.css']
})
export class ColaComponent implements OnInit {
    
  orders: Order[];
  products: Product[];
  orderProrducts: [string][number];

  constructor(private orderService: OrderService,private helper: HelperService,
    private productService: ProductsService) 
  {
    this.orders = [];
  }

  ngOnInit(): void {
    this.helper.customMessage.subscribe(msg => this.orders = msg);
    this.orderService.findAll().subscribe(data => {
      this.orders = data;      
    }); 
  }

}
