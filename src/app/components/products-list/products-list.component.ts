import { Component, OnInit } from '@angular/core';
import { Product } from '../../models/product'
import { ProductsService } from '../../services/products.service'

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {

  products: Product[];

  constructor(private productsService: ProductsService) {}

  ngOnInit() {
    this.productsService.findAll().subscribe(data => {
      this.products = data;
    })
  }

}
