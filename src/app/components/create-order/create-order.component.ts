import { Component, OnInit } from '@angular/core';
import { Product } from '../../models/product'
import { ProductsService } from '../../services/products.service'
import { OrderService } from '../../services/order.service'
import { Order } from '../../models/order';
import { HelperService } from '../../services/HelperService'
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.css']
})
export class CreateOrderComponent implements OnInit {

  newOrders: Order[];
  order: Order;
  products: Product[];
  toAdd: products[];

  constructor(private productsService: ProductsService,
    private orderService: OrderService,private helper: HelperService) {
      this.order = new Order();
      this.newOrders = [];
      this.toAdd = [];
    }

  addProduct(product: Product)
  {
    this.toAdd.push(product);
    this.products.splice(this.products.indexOf(product),1);
  }

  changeMessage(orders: Order[])
  {    
    this.helper.changeMessage(orders);    
  }

  createOrder()
  {    
    this.order.state = "cola";
    this.order.products = this.toAdd;
    this.orderService.save(this.order).subscribe(data => this.succesfull());   
    this.orderService.findAll().subscribe(data => {           
    this.changeMessage(data);
    });
    this.toAdd = [];
    this.loadProducts();
  }  

  succesfull()
  {
    alert("Order Created");
  }

  loadProducts()
  {
    this.productsService.findAll().subscribe(data => {
    this.products = data;})
  }

  ngOnInit() {
    this.helper.customMessage.subscribe(msg => this.newOrders = msg);
    this.productsService.findAll().subscribe(data => {
    })
  }

}
