import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparacionListComponent } from './preparacion-list.component';

describe('PreparacionListComponent', () => {
  let component: PreparacionListComponent;
  let fixture: ComponentFixture<PreparacionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreparacionListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreparacionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
