import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntregadoListComponent } from './entregado-list.component';

describe('EntregadoListComponent', () => {
  let component: EntregadoListComponent;
  let fixture: ComponentFixture<EntregadoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntregadoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntregadoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
