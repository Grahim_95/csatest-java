import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Order } from '../models/order'
@Injectable()
export class HelperService {
  private message = new BehaviorSubject<Order[]>([]);
  public customMessage = this.message.asObservable();
  constructor() {}
  public changeMessage(msg: Order[]): void {
    this.message.next(msg);
  }
}