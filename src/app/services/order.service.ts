import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Order } from '../models/order';
import { Product } from '../models/product'
import { Observable } from 'rxjs';

@Injectable()
export class OrderService
{
    private orderUrl :string;

    constructor(private http: HttpClient)
    {
        this.orderUrl = 'http://localhost:8080/orders';
    }

    public findAll(): Observable<Order[]>
    {
        return this.http.get<Order[]>(this.orderUrl);
    }

    public save(order: Order)
    {        
        return this.http.post<Order>(this.orderUrl,order);
    }
}